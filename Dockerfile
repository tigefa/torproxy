FROM tigefa/bionic

# Metadata params
ARG BUILD_DATE
ARG VERSION
ARG VCS_URL
ARG VCS_REF

# Metadata
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="Tor Proxy" \
      org.label-schema.description="Tor with Polipo proxy" \
      org.label-schema.url="https://tigefa.gitlab.io" \
      org.label-schema.vcs-url=$VCS_URL \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vendor="Sugeng Tigefa" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0" \
      maintainer="Sugeng Tigefa <tigefa@gmail.com>"

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Update and Upgrade
RUN apt-get update -yqq && apt-get upgrade -yqq -o Dpkg::Options::="--force-confold"

# Install tor
RUN echo "deb [trusted=yes] https://deb.torproject.org/torproject.org bionic main" | tee /etc/apt/sources.list.d/tor.list && \
    echo "deb-src [trusted=yes] https://deb.torproject.org/torproject.org bionic main" | tee -a /etc/apt/sources.list.d/tor.list
RUN apt-get update -yqq && \
    apt-get -yqq install curl wget tor deb.torproject.org-keyring && \
    /sbin/install_clean polipo

# Add tor and polipo in my_init startup
RUN mkdir -p /etc/my_init.d
COPY torproxy.sh /etc/my_init.d/torproxy.sh
COPY config-polipo /etc/polipo/config
RUN chmod +x /etc/my_init.d/torproxy.sh

# expose tor port
EXPOSE 8123 9050

HEALTHCHECK --interval=60s --timeout=15s --start-period=90s \
            CMD curl --socks5-hostname localhost:9050 -L 'https://api.ipify.org'

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
