## torproxy

Tor proxy with polipo on port 8123

```bash
docker run -d --restart=always --name=torproxy -p 8123:8123 -p 9050:9050 registry.gitlab.com/tigefa/torproxy
```

use it on browser

`http://127.0.0.1:8123`

or on terminal

```bash
export http_proxy="http://127.0.0.1:8123"
export https_proxy="http://127.0.0.1:8123"
```
